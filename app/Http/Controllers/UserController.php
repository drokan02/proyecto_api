<?php

namespace App\Http\Controllers;

use App\Models\User;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    public function downloadPdf()
    {
        $users = User::all();

        view()->share('users.pdf', $users);

        $pdf = Pdf::loadView('users.pdf', ['users' => $users]);

        return $pdf->download('users.pdf');
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
